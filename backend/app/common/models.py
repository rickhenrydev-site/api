from datetime import datetime
from typing import List, Optional

from pydantic import BaseModel, Schema, UrlStr


class NamedURL(BaseModel):
    name: str = Schema(..., title="Name for Resource")
    endpoint: str = Schema(
        ..., title="Endpoint", descritpion="The location of the resource"
    )


class Project(BaseModel):
    name: str = Schema(..., title="Project Name")
    description: str = Schema(..., title="Project description")
    deployments: Optional[List[NamedURL]] = Schema(None, title="Deployments")
    gitlab_project_ids: Optional[List[str]] = Schema(None, title="Gitlab Project Ids")
    documentation: Optional[List[NamedURL]] = Schema(None, title="Documentation")
    associated_resources: Optional[List[NamedURL]] = Schema(
        None, title="Associated Resources"
    )


class Commit(BaseModel):
    title: str = Schema(..., title="Title")
    short_id: str = Schema(..., title="Short ID")
    created_at: datetime = Schema(..., title="Created At")
    message: str = Schema(..., title="Commit Message")
    author: str = Schema(..., title="Author")


class Milestone(BaseModel):
    title: str = Schema(..., title="Title")
    description: str = Schema(..., title="Description")
    start_date: Optional[datetime] = Schema(None, title="Start Date")
    due_date: Optional[datetime] = Schema(None, title="Due Date")
    state: str = Schema("Unknown", title="State")
    url: UrlStr = Schema(..., title="View on GitLab")


class ProjectOut(Project):
    latest_commits: List[Commit] = Schema(None, title="Latest Commits")
    code: Optional[List[NamedURL]] = Schema(None, title="Code Repos")
    milestones: Optional[List[Milestone]] = Schema(None, title="Project Milestones")
