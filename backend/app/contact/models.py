from enum import Enum

from pydantic import BaseModel, Schema, EmailStr


class StatusResponses(str, Enum):
    success: str = "SUCCESS"
    failure: str = "FAILURE"


class ContactResponse(BaseModel):
    status: StatusResponses = Schema(..., title="Status of request")
    message: str = Schema(..., title="Response Message from Server")


# TODO: refactor to auto inject client info without requiring from frontend
class ContactData(BaseModel):
    name: str = Schema(..., title="Sender Name")
    email: EmailStr = Schema(..., title="Sender Email")
    subject: str = Schema(..., title="Email Subject")
    message: str = Schema(..., title="Message Body")
