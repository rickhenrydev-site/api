import os

from fastapi import APIRouter, Security, Depends

from app.auth import security
from app.auth.models import User
from app.contact import models
from app.dependencies import send_email

contact_router = APIRouter()

FROM_ADDRESS = os.getenv("MAILGUN_CONTACT_FROM_ADDRESS")
TO_ADDRESS = os.getenv("CONTACT_TO")


@contact_router.post("/public", response_model=models.ContactResponse)
async def public_contact(data: models.ContactData):
    """Send email from the contact form on the public section of my website."""
    await send_email(
        TO_ADDRESS, data.subject, data.message, FROM_ADDRESS, data.name, data.email
    )
    return models.ContactResponse(
        status=models.StatusResponses.success, message="Message sent"
    )


# TODO: refactor to auto inject client info without requiring from frontend
@contact_router.post("/client", response_model=models.ContactResponse)
async def client_contact(
    data: models.ContactData,
    _current_user: User = Depends(security.get_current_active_user),
):
    """Clients send email with flag that they are from a client."""
    await send_email(
        TO_ADDRESS,
        data.subject,
        data.message,
        FROM_ADDRESS,
        data.name,
        data.email,
        True,
    )
    return models.ContactResponse(
        status=models.StatusResponses.success, message="Message sent"
    )
