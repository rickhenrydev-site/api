import os
from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware

from app.auth.router import auth_router
from app.contact.router import contact_router
from app.dependencies import db

app = FastAPI(title="rickhenry.dev API", version="19.9.0")


@app.on_event("startup")
async def setup_db():
    await db.users.create_index("email", unique=True)


app.include_router(
    auth_router,
    prefix="/auth",
    tags=["auth"],
    responses={401: {"description": "Authentication Failure"}},
)

app.include_router(contact_router, prefix="/contact", tags=["contact"])

app.add_middleware(
    CORSMiddleware,
    allow_credentials=True,
    allow_origin_regex=os.getenv("CORS_REGEX", ".*localhost.*"),
    allow_methods=["*"],
    allow_headers=["*"],
)
