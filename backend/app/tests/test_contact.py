import os

import pytest
from _pytest.monkeypatch import MonkeyPatch
from starlette.testclient import TestClient


def test_contact_public(test_client: TestClient, monkeypatch: MonkeyPatch):
    message_data = {
        "name": "Test Sender",
        "email": "test@example.com",
        "subject": "Some Inquiry",
        "message": "I would like to inquire about something\nTestPerson",
    }

    async def fake_send_email(to, subject, text, from_address, from_name, reply_to):
        assert to == "rickhenry@rickhenry.dev"
        assert subject == message_data["subject"]
        assert text == message_data["message"]
        assert from_address == "contact@mg.rickhenry.dev"
        assert from_name == message_data["name"]
        assert reply_to == message_data["email"]

    monkeypatch.setattr("app.contact.router.send_email", fake_send_email)

    response = test_client.post("/contact/public", json=message_data)

    assert response.status_code == 200
    assert response.json()["status"] == "SUCCESS"
    assert response.json()["message"] == "Message sent"


def test_not_logged_in_client_send_fails(
    test_client: TestClient, monkeypatch: MonkeyPatch
):
    message_data = {
        "name": "Test Sender",
        "email": "test@example.com",
        "subject": "Some Inquiry",
        "message": "I would like to inquire about something\nTestPerson",
    }

    async def fake_send_email(*args):
        assert False, "Should not have been called"

    monkeypatch.setattr("app.contact.router.send_email", fake_send_email)

    response = test_client.post("/contact/client", json=message_data)

    assert response.status_code == 401


def test_logged_in_client_send(
    authenticated_client: TestClient, monkeypatch: MonkeyPatch, async_db
):
    message_data = {
        "name": "Test Sender",
        "email": "test@example.com",
        "subject": "A Problem",
        "message": "There is a problem with my project that requires "
        "immediate attention.",
    }

    class FakeSendEmail:
        def __init__(self):
            self.called = False

        async def __call__(
            self, to, subject, text, from_address, from_name, reply_to, high_priority
        ):
            self.called = True
            assert to == "rickhenry@rickhenry.dev"
            assert subject == message_data["subject"]
            assert text == message_data["message"]
            assert from_address == "contact@mg.rickhenry.dev"
            assert from_name == message_data["name"]
            assert reply_to == message_data["email"]
            assert high_priority is True

    fake_send_email = FakeSendEmail()

    monkeypatch.setattr("app.contact.router.send_email", fake_send_email)

    response = authenticated_client.post("/contact/client", json=message_data)

    assert response.status_code == 200
    assert response.json()["status"] == "SUCCESS"
    assert response.json()["message"] == "Message sent"
    assert fake_send_email.called
