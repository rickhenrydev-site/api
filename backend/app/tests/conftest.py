import os
import uuid
from urllib.parse import quote_plus

import pymongo
import pytest
from motor import motor_asyncio

from starlette.testclient import TestClient

from app.auth import security
from app.main import app


@pytest.fixture
def db_name():
    return str(uuid.uuid4())


@pytest.fixture
def db(db_name):
    db_uri = "mongodb://{username}:{password}@{host}:{port}".format(
        username=quote_plus(os.getenv("MONGO_INITDB_ROOT_USERNAME", "root")),
        password=quote_plus(os.getenv("MONGO_INITDB_ROOT_PASSWORD", "root")),
        host=quote_plus(os.getenv("DB_HOST", "localhost")),
        port=quote_plus(os.getenv("DB_PORT", "27017")),
    )
    db_client = pymongo.MongoClient(db_uri)
    return db_client[db_name]


@pytest.fixture
def async_db(db_name):
    db_uri = "mongodb://{username}:{password}@{host}:{port}".format(
        username=quote_plus(os.getenv("MONGO_INITDB_ROOT_USERNAME", "root")),
        password=quote_plus(os.getenv("MONGO_INITDB_ROOT_PASSWORD", "root")),
        host=quote_plus(os.getenv("DB_HOST", "localhost")),
        port=quote_plus(os.getenv("DB_PORT", "27017")),
    )
    db_client = motor_asyncio.AsyncIOMotorClient(db_uri)
    return db_client[db_name]


@pytest.fixture
def test_client():
    return TestClient(app)


@pytest.fixture
def admin_authenticated_client(test_client, admin_user, async_db, monkeypatch):
    monkeypatch.setattr("app.auth.crud.db", async_db)
    otp = security.generate_otp(admin_user["email"])
    test_client.post("/auth/confirm", json={"email": admin_user["email"], "code": otp})

    return test_client


@pytest.fixture
def authenticated_client(test_client, user1, async_db, monkeypatch):
    monkeypatch.setattr("app.auth.crud.db", async_db)
    otp = security.generate_otp(user1["email"])
    test_client.post("/auth/confirm", json={"email": user1["email"], "code": otp})

    return test_client


@pytest.fixture
def user_data():
    return {"email": "test@rickhenry.dev", "full_name": "Test Person", "role": "CLIENT"}


@pytest.fixture
def user1(db, user_data):
    result = db.users.insert_one(user_data)
    return {**user_data, "id": result.inserted_id}
