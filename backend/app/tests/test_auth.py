import os
import secrets

import pytest
from _pytest.monkeypatch import MonkeyPatch
from starlette.testclient import TestClient

from app.auth import security
from app.auth.models import UserInDB, AuthRole


@pytest.fixture
def user_data2():
    return {
        "email": "test2@rickhenry.dev",
        "full_name": "Test Person 2",
        "role": "CLIENT",
    }


@pytest.fixture
def user2(db, user_data2):
    result = db.users.insert_one(user_data2)
    return {**user_data2, "id": result.inserted_id}


@pytest.fixture
def disabled_user(db):
    data = {"email": "disabled@rickhenry.dev", "disabled": True}
    result = db.users.insert_one(data)
    return {**data, "id": result.inserted_id}


@pytest.fixture
def admin_user(db):
    new_user = UserInDB(
        email="admin@rickhenry.dev", full_name="Admin", role=AuthRole.admin
    )
    result = db.users.insert_one(new_user.dict())
    return {**new_user.dict(), "id": result.inserted_id}


def test_create_user(
    admin_authenticated_client: TestClient,
    user_data: dict,
    monkeypatch: MonkeyPatch,
    async_db,
    db,
):
    """Test creating a new user"""
    monkeypatch.setattr("app.auth.crud.db", async_db)
    response = admin_authenticated_client.post("/auth/users", json=user_data)

    assert response.status_code == 201
    user_in_db = db.users.find_one({"email": user_data["email"]})

    assert user_in_db["full_name"] == user_data["full_name"]


def test_create_user_existing_fails(
    admin_authenticated_client: TestClient,
    user_data: dict,
    user1: dict,
    monkeypatch: MonkeyPatch,
    async_db,
):
    monkeypatch.setattr("app.auth.crud.db", async_db)
    response = admin_authenticated_client.post(
        "/auth/users", json={"email": user_data["email"]}
    )

    assert response.status_code == 400


def test_create_user_not_logged_in_fails(
    test_client: TestClient, monkeypatch: MonkeyPatch, async_db, user_data: dict
):
    monkeypatch.setattr("app.auth.crud.db", async_db)
    response = test_client.post("/auth/users", json=user_data)

    assert response.status_code == 401


def test_create_user_not_admin_fails(
    authenticated_client: TestClient,
    monkeypatch: MonkeyPatch,
    async_db,
    user_data2: dict,
):
    monkeypatch.setattr("app.auth.crud.db", async_db)
    response = authenticated_client.post("/auth/users", json=user_data2)

    assert response.status_code == 403


def test_request_login(
    test_client: TestClient, user1: dict, monkeypatch: MonkeyPatch, async_db
):
    """Test request login sends email"""
    monkeypatch.setattr("app.auth.crud.db", async_db)
    monkeypatch.setattr(secrets, "choice", lambda args: "1")

    async def fake_send_email(to, subject, text, from_address, from_name):
        assert to == "test@rickhenry.dev"
        assert subject == "Your One Time Password"
        assert text == "Your password is 11111111"
        assert from_address == "login@mg.rickhenry.dev"
        assert from_name == "Client Portal"

    monkeypatch.setattr("app.auth.router.send_email", fake_send_email)
    response = test_client.post("/auth/request", json={"email": user1["email"]})
    assert response.status_code == 200
    assert response.text == '"Please check your email for a single use password."'


def test_request_login_disabled_fails(
    test_client: TestClient, disabled_user: dict, monkeypatch: MonkeyPatch, async_db
):
    monkeypatch.setattr("app.auth.crud.db", async_db)

    async def fake_send_email(*_args, **_kwargs):
        assert False, "Should not have been called"

    monkeypatch.setattr("app.auth.router.send_email", fake_send_email)
    response = test_client.post("/auth/request", json={"email": disabled_user["email"]})
    assert response.status_code == 401


def test_request_magic_link(
    test_client: TestClient, user1: dict, monkeypatch: MonkeyPatch, async_db
):
    monkeypatch.setattr("app.auth.crud.db", async_db)
    monkeypatch.setattr(secrets, "token_urlsafe", lambda: "123456789")
    hostname = os.getenv("HOSTNAME", "localhost")

    async def fake_send_email(to, subject, text, from_address, from_name):
        assert to == "test@rickhenry.dev"
        assert subject == "Your magic sign in link"
        assert text == f"Click this link to sign in\n{hostname}?secret=123456789"
        assert from_address == "login@mg.rickhenry.dev"
        assert from_name == "Client Portal"

    monkeypatch.setattr("app.auth.router.send_email", fake_send_email)
    response = test_client.post("/auth/request-magic", json={"email": user1["email"]})

    assert response.status_code == 200
    assert response.text == '"Please check your email for your sign in link."'


def test_request_magic_disabled_fails(
    test_client: TestClient, disabled_user: dict, monkeypatch: MonkeyPatch, async_db
):
    monkeypatch.setattr("app.auth.crud.db", async_db)

    async def fake_send_email(*_args, **_kwargs):
        assert False, "Should not have been called"

    monkeypatch.setattr("app.auth.router.send_email", fake_send_email)
    response = test_client.post(
        "/auth/request-magic", json={"email": disabled_user["email"]}
    )
    assert response.status_code == 401


def test_confirm_login(
    test_client: TestClient, user1: dict, monkeypatch: MonkeyPatch, async_db
):
    monkeypatch.setattr("app.auth.crud.db", async_db)
    otp = security.generate_otp(user1["email"])

    response = test_client.post(
        "/auth/confirm", json={"email": user1["email"], "code": otp}
    )

    assert response.status_code == 200
    assert response.cookies.get("token") is not None


def test_confirm_login_wrong_code_fails(
    test_client: TestClient, user1: dict, monkeypatch: MonkeyPatch, async_db
):
    monkeypatch.setattr("app.auth.crud.db", async_db)
    _otp = security.generate_otp(user1["email"])

    response = test_client.post(
        "/auth/confirm", json={"email": user1["email"], "code": "123456"}
    )

    assert response.status_code == 400
    assert response.cookies.get("token") is None


def test_confirm_login_wrong_email_fails(
    test_client: TestClient, user1: dict, monkeypatch: MonkeyPatch, async_db
):
    monkeypatch.setattr("app.auth.crud.db", async_db)
    otp = security.generate_otp(user1["email"])

    response = test_client.post(
        "/auth/confirm", json={"email": "fail@rickhenry.dev", "code": otp}
    )

    assert response.status_code == 400
    assert response.cookies.get("token") is None


def test_confirm_magic(
    test_client: TestClient, user1: dict, monkeypatch: MonkeyPatch, async_db
):
    monkeypatch.setattr("app.auth.crud.db", async_db)
    magic_url = security.generate_magic_link(user1["email"])
    url_secret = magic_url.split("=")[-1]
    response = test_client.post(
        "/auth/confirm-magic", json={"email": user1["email"], "secret": url_secret}
    )

    assert response.status_code == 200
    assert response.cookies.get("token") is not None


def test_confirm_magic_wrong_email_fails(
    test_client: TestClient, user1: dict, monkeypatch: MonkeyPatch, async_db
):
    monkeypatch.setattr("app.auth.crud.db", async_db)
    magic_url = security.generate_magic_link(user1["email"])
    url_secret = magic_url.split("=")[-1]
    response = test_client.post(
        "/auth/confirm-magic",
        json={"email": "fail@rickhenry.dev", "secret": url_secret},
    )

    assert response.status_code == 400
    assert response.cookies.get("token") is None


def test_confirm_magic_wrong_secret_fails(
    test_client: TestClient, user1: dict, monkeypatch: MonkeyPatch, async_db
):
    monkeypatch.setattr("app.auth.crud.db", async_db)
    magic_url = security.generate_magic_link(user1["email"])
    response = test_client.post(
        "/auth/confirm-magic", json={"email": user1["email"], "secret": "123456789"}
    )

    assert response.status_code == 400
    assert response.cookies.get("token") is None


def test_logged_in_get_user_info(
    test_client: TestClient, user1: dict, monkeypatch: MonkeyPatch, async_db
):
    monkeypatch.setattr("app.auth.crud.db", async_db)
    otp = security.generate_otp(user1["email"])

    _response = test_client.post(
        "/auth/confirm", json={"email": user1["email"], "code": otp}
    )

    response2 = test_client.get("/auth/me")

    assert response2.status_code == 200


def test_not_logged_in_cant_get_user_info(
    test_client: TestClient, monkeypatch: MonkeyPatch, async_db
):
    monkeypatch.setattr("app.auth.crud.db", async_db)
    response = test_client.get("/auth/me")

    assert response.status_code == 401


def test_get_user_has_role_info(authenticated_client: TestClient):
    response = authenticated_client.get("/auth/me")

    assert response.status_code == 200
    assert response.json()["role"] == "CLIENT"


def test_get_user_has_role_info_admin(admin_authenticated_client: TestClient):
    response = admin_authenticated_client.get("/auth/me")

    assert response.status_code == 200
    assert response.json()["role"] == "ADMIN"


def test_log_out(authenticated_client: TestClient, user1: dict):
    response2 = authenticated_client.get("/auth/sign-out")

    assert response2.status_code == 200
    assert response2.cookies.get("token") == '""'

    response3 = authenticated_client.get("/auth/me")

    assert response3.status_code == 401


def test_get_all_users(
    admin_authenticated_client: TestClient,
    admin_user: dict,
    # monkeypatch: MonkeyPatch,
    # async_db,
    user1: dict,
):
    response = admin_authenticated_client.get("/auth/users/all")

    assert response.status_code == 200
    emails = [item["email"] for item in response.json()]
    assert admin_user["email"] in emails
    assert user1["email"] in emails


def test_get_all_users_fails_if_not_admin(authenticated_client: TestClient):
    response = authenticated_client.get("/auth/users/all")

    assert response.status_code == 403


def test_authenticated_user_partial_update_info(
    authenticated_client: TestClient, monkeypatch: MonkeyPatch, async_db, user1: dict
):
    new_data = {
        "full_name": "Updated Name",
        "email": user1["email"],
        "company_name": "Updated Company",
        "disabled": False,
    }

    response = authenticated_client.put("/auth/me", json=new_data)

    assert response.status_code == 200

    response2 = authenticated_client.get("/auth/me")

    assert response2.status_code == 200
    assert response2.json()["email"] == new_data["email"]
    assert response2.json()["full_name"] == new_data["full_name"]
    assert response2.json()["company_name"] == new_data["company_name"]


def test_authenticated_user_update_info(
    authenticated_client: TestClient,
    monkeypatch: MonkeyPatch,
    async_db,
    db,
    user1: dict,
):
    new_data = {
        "full_name": "Updated Name",
        "email": "updated@rickhenry.dev",
        "company_name": "Updated Company",
        "disabled": False,
    }

    response = authenticated_client.put("/auth/me", json=new_data)

    assert response.status_code == 200

    assert db.users.find_one({"email": new_data["email"]}) is not None

    # need to re-authenticate after email change
    otp = security.generate_otp(new_data["email"])
    authenticated_client.post(
        "/auth/confirm", json={"email": new_data["email"], "code": otp}
    )

    response2 = authenticated_client.get("/auth/me")

    assert response2.status_code == 200
    assert response2.json()["email"] == new_data["email"]
    assert response2.json()["full_name"] == new_data["full_name"]
    assert response2.json()["company_name"] == new_data["company_name"]


def test_admin_get_user(admin_authenticated_client: TestClient, user1: dict):
    response = admin_authenticated_client.get(f"/auth/users/{user1['email']}")

    assert response.status_code == 200
    assert response.json()["email"] == user1["email"]
    assert response.json()["full_name"] == user1["full_name"]


def test_admin_update_user(
    admin_authenticated_client: TestClient, monkeypatch: MonkeyPatch, db, user1: dict
):
    new_data = {
        "full_name": "Updated Name",
        "email": "updated@rickhenry.dev",
        "company_name": "Updated Company",
        "disabled": False,
        "role": "CLIENT",
    }

    response = admin_authenticated_client.put(
        f"/auth/users/{user1['email']}", json=new_data
    )

    assert response.status_code == 200

    response2 = admin_authenticated_client.get(f"/auth/users/{new_data['email']}")

    assert response2.status_code == 200
    assert response2.json()["email"] == new_data["email"]
    assert response2.json()["full_name"] == new_data["full_name"]
    assert response2.json()["company_name"] == new_data["company_name"]
    assert response2.json()["disabled"] == new_data["disabled"]
    assert response2.json()["role"] == new_data["role"]


def test_admin_disable_user(
    admin_authenticated_client: TestClient,
    monkeypatch: MonkeyPatch,
    db,
    user1: dict,
    test_client: TestClient,
):
    new_data = {
        "full_name": user1["full_name"],
        "email": user1["email"],
        "company_name": None,
        "disabled": True,
        "role": user1["role"],
    }

    response = admin_authenticated_client.put(
        f"/auth/users/{user1['email']}", json=new_data
    )

    assert response.status_code == 200

    response2 = admin_authenticated_client.get(f"/auth/users/{new_data['email']}")

    assert response2.status_code == 200
    assert response2.json()["email"] == new_data["email"]
    assert response2.json()["full_name"] == new_data["full_name"]
    assert response2.json()["company_name"] == new_data["company_name"]
    assert response2.json()["disabled"] is True
    assert response2.json()["role"] == new_data["role"]

    response3 = test_client.post("/auth/request", json={"email": new_data["email"]})

    assert response3.status_code == 401


#
def test_admin_change_user_role(
    admin_authenticated_client: TestClient,
    monkeypatch: MonkeyPatch,
    db,
    user1: dict,
    test_client: TestClient,
):
    new_data = {
        "full_name": user1["full_name"],
        "email": user1["email"],
        "company_name": None,
        "disabled": False,
        "role": "ADMIN",
    }

    response = admin_authenticated_client.put(
        f"/auth/users/{user1['email']}", json=new_data
    )

    assert response.status_code == 200

    response2 = admin_authenticated_client.get(f"/auth/users/{new_data['email']}")

    assert response2.status_code == 200
    assert response2.json()["email"] == new_data["email"]
    assert response2.json()["full_name"] == new_data["full_name"]
    assert response2.json()["company_name"] == new_data["company_name"]
    assert response2.json()["disabled"] is False
    assert response2.json()["role"] == new_data["role"]

    otp = security.generate_otp(new_data["email"])
    test_client.post("/auth/confirm", json={"email": new_data["email"], "code": otp})

    response3 = test_client.get("/auth/users/all")

    assert response3.status_code == 200, "User can now get protected route"


def test_non_admin_cannot_update_other_user(
    authenticated_client: TestClient, user2: dict
):
    new_data = {
        "full_name": "Updated Name",
        "email": "updated@rickhenry.dev",
        "company_name": "Updated Company",
        "disabled": False,
        "role": "CLIENT",
    }
    response = authenticated_client.put(f"/auth/users/{user2['email']}", json=new_data)

    assert response.status_code == 403


def test_admin_delete_user(admin_authenticated_client: TestClient, user1: dict):
    response = admin_authenticated_client.delete(f"/auth/users/{user1['email']}")

    assert response.status_code == 200
    assert response.text == '"User Deleted"'

    response2 = admin_authenticated_client.get(f"/auth/users/{user1['email']}")
    print(response2.json())

    assert response2.status_code == 404


def test_non_admin_cannot_delete_user(
    authenticated_client: TestClient, user1: dict, user2: dict, db
):
    user1_db = db.users.find_one({"_id": user1["id"]})
    print(user1_db)
    response = authenticated_client.delete(f"/auth/users/{user2['email']}")

    assert response.status_code == 403

    assert db.users.find_one({"email": user2["email"]}) is not None
